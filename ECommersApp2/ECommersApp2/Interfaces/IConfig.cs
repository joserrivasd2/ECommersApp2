﻿//PARA LA IMPLEMENTACION DE LA BD EN CADA PLATAFORMA
using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Interop;
namespace ECommersApp2.Interfaces
{
    public interface IConfig
    {
        string DirectoryDB { get; }
        ISQLitePlatform Platform { get; }       
    }
}
