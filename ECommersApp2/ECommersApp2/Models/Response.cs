﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECommersApp2.Models
{
   public class Response
    {
        public bool  IsSucces { get; set; }
        public string Message { get; set; }
        //Propiedad que devolvera resultado de Consulta
        public object Result { get; set; }
    }
}
