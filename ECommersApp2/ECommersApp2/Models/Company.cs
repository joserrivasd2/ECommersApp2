﻿namespace ECommersApp2.Models
{
    using SQLite;
    using SQLiteNetExtensions.Attributes;
    using System.Collections.Generic;
    public class Company
    {
        [PrimaryKey]
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Logo { get; set; }
        public int DepartmentId { get; set; }
        public int CityId { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<User> Users { get; set; }


        //Esta clase la tendran todas las clases que mandemos a bases de datos
        public override int GetHashCode()
        {
            return CompanyId;
        }
    }
}
