﻿using ECommersApp2.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ECommersApp2
{
	public partial class App : Application
	{

        #region Properties
        // PROPIEDADES AUTOIMPLEMENTADAS (NO NECESITO DECLARAR EL ATRIBUTO)
        public static NavigationPage Navigator { get; internal set; }
        public static MasterPage Master { get; internal set; }
        #endregion

        #region Constructores
        public App()
        {
            InitializeComponent();
            // The root page of your application
            MainPage = new LoginPage();

        }
        #endregion

        #region Metodos
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        #endregion
    }
    }
