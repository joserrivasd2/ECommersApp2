﻿
using ECommersApp2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ECommersApp2.Services
{
    public class ApiService
    {

        public async Task<Response> Login(string email, string password)
        {
            try
            {
                // login request parametros a enviar necesarios para obtener una respuesta del servicio
                var loginRequest = new LoginRequest
                {
                    Email = email,
                    Password = password,
                };
                //serializando objeto y convirtiendolo en Content

                var request = JsonConvert.SerializeObject(loginRequest);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var cliente = new HttpClient();
                cliente.BaseAddress = new Uri("http://zulu-software.com");
                var url = "/ECommerce/api/Users/Login";
                // para ejecutar
                var response = await cliente.PostAsync(url, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucces = false,
                        Message = "Usuario o Contraseña Incorrectos",
                    };
                }

                // si llega hasta aqui hubo respuesta hay q leerla y deserializarla
                var result = await response.Content.ReadAsStringAsync();
                // deserializar un objeto de la clase USer q esta en la variable result
                var user = JsonConvert.DeserializeObject<User>(result);

                return new Response
                {
                    IsSucces = true,
                    Message = "Login Ok",
                    //Regresamos tambien el objeto user que ya tenemos deserializado
                    Result = user,
                };

            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucces = false,
                    Message = ex.Message,
                };
            }
        }

    }
}
