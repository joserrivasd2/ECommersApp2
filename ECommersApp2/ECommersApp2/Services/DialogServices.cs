﻿
namespace ECommersApp2.Services
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    public class DialogServices
    {
        public async Task ShowMessage(string title, string message)
        {
            await App.Current.MainPage.DisplayAlert(
                title, message, "Aceptar"
                ); 
          }
    }
}
