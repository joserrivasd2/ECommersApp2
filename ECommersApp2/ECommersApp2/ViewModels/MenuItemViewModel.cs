﻿namespace ECommersApp2.ViewModels
{
    using ECommersApp2.Services;
    using GalaSoft.MvvmLight.Command;
    using System;
    using System.Windows.Input;

    public class MenuItemViewModel
    {
        #region Atributos
        private NavigationService navigationService;
        #endregion
        
        #region Propiedades
        public string Icon { get; set; }

        public string Title { get; set; }

        public string PageName { get; set; }
        #endregion

        #region Command
        public ICommand NavigateCommand { get { return new RelayCommand(Navigate); } }
        
        private async void Navigate()
        {
            await navigationService.Navigate(PageName);
        }
        #endregion

        #region Constructores
        public MenuItemViewModel()
        {
            navigationService = new NavigationService();

        }
        #endregion

    }
}
