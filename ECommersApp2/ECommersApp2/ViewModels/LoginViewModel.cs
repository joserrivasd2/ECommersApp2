﻿using ECommersApp2.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;

namespace ECommersApp2.ViewModels
{
    // heredamos de la interfaz INotifyPropertyChanged para refrescar el ACTIVI INDICATOR u otra propiedad
    public class LoginViewModel : INotifyPropertyChanged
    {

        #region Atributos
        private NavigationService navigationService;
        private DialogServices dialogServices;
        // para utilizar el apiServices
        private ApiService apiService;
        private bool isRunning;
        #endregion

        #region Constructor
        public LoginViewModel()
        {
            navigationService = new NavigationService();
            dialogServices = new DialogServices();
            apiService = new ApiService();
            IsRemembered = true;
        }
        #endregion

        #region Eventos
        // implementamos la interfaz INotifyPropertyChanged
        // ESTE EVENTO NOS SIRVE PARA DECIRLE CUANDO CAMBIO LA PROPIEDAD
        public event PropertyChangedEventHandler PropertyChanged; 
        #endregion

        #region Propiedades
        public string User { get; set; }
        public string Password { get; set; }
        public bool IsRemembered { get; set; }
        public bool IsRunning
        {
            set {
                if (isRunning != value)
                    isRunning = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }

        }
        #endregion

        #region Commands
        public ICommand LoginCommand { get { return new RelayCommand(Login); } }
        
        private async void Login()
        {
            if (string.IsNullOrEmpty(User))
            {
                await dialogServices.ShowMessage("Error", "Debes Ingresar un Usuario");
                return;
            }
            if (string.IsNullOrEmpty(Password))
            {
                await dialogServices.ShowMessage("Error", "Debes Ingresar una Contraseña");
                return;
            }
            IsRunning = true;
            var response = await apiService.Login(User, Password);
            IsRunning = false;
            if (!response.IsSucces)
            {
                await dialogServices.ShowMessage("Error", response.Message);
                return;
            }

            navigationService.SetMaintPage();
        } 
        #endregion
    }
}
